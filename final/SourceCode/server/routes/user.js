const express = require('express')
const cryptoJS = require('crypto-js')
const db = require('../db')
const utils = require('../utils')
const jwt = require('jsonwebtoken')
const config = require('../config')

const router = express.Router()
//id | firstName | lastName | email  | phone  | birthDate  | password  | city   | pincode | gender | createdTimestamp
router.post('/signup', (request, response) => {
  const {
    firstName,
    lastName,
    email,
    phone,
    birthDate,
    password,
    city,
    pincode,
    gender,
  } = request.body

  console.log(birthDate)

  const encryptedPassword = String(cryptoJS.MD5(password))

  const statement = `
    INSERT INTO user (firstName, lastName, email, phone, birthDate, password, city, pincode, gender) 
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
  `
  db.pool.query(
    statement,
    [
      firstName,
      lastName,
      email,
      phone,
      birthDate,
      encryptedPassword,
      city,
      pincode,
      gender,
    ],
    (error, result) => {
      response.send(utils.createResult(error, result))
    }
  )
})

router.post('/signin', (request, response) => {
  const { password, email } = request.body

  const encryptedPassword = String(cryptoJS.MD5(password))

  const statement = `
      SELECT user_id, firstName, lastName, email 
      FROM user
      WHERE email = ? AND password = ?
  `
  db.pool.query(statement, [email, encryptedPassword], (error, users) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else {
      if (users.length === 0) {
        result['status'] = 'error'
        result['error'] = 'user does not exist'
      } else {
        const user = users[0]
        result['status'] = 'success'
        const payload = { user_id: user['user_id'] }
        const token = jwt.sign(payload, config.secret)
        result['data'] = {
          firstName: user['firstName'],
          lastName: user['lastName'],
          email: user['email'],
          token,
        }
      }
    }
    response.send(result)
  })
})

router.get('/userDetails', (request, response) => {
  const statement = `
     select firstName,lastName,email,phone,gender,city from user where user_id=?
  `
  db.pool.query(statement, [request.user_id], (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.post('/book_ticket', (request, response) => {
  const { journey_date, _scheduleId } = request.body
  console.log('book ticket' + journey_date)

  const statement = `
  INSERT INTO ticket( journey_date, user_id, schedule_id) 
  VALUES (?,?,?);
  `
  db.pool.query(
    statement,
    [journey_date, request.user_id, _scheduleId],
    (error, result) => {
      response.send(utils.createResult(error, result))
    }
  )
})

router.get('/show_allTicket', (request, response) => {
  const statement = `
      select u.firstName,u.lastName,d.source,d.destination,d.fare,
      s.schedule_id,s.departue_time,s.arrival_time,t.ticket_id,
      t.journey_date,t.createdTimestamp as "Booking date and time" from ticket t 
      inner join train_schedule s on s.schedule_id=t.schedule_id
      inner join train_data d on d.train_id=s.train_id
      inner join user u on u.user_id=t.user_id
      where u.user_id=?;
  `
  db.pool.query(statement, [request.user_id], (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/cancel_ticket:_ticketId', (request, response) => {
  const { _ticketId } = request.params
  console.log(_ticketId)

  const statement = `
     Delete from ticket where ticket_id=? AND user_id=?
  `
  db.pool.query(statement, [_ticketId, request.user_id], (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

// // router.post('/scheduled_train', (request, response) => {
// //   const { _journeyDate, _from, _to } = request.body

// //   console.log(_journeyDate)
// //   const statement = `
// //   select d.source , d.destination, d.fare ,s.departue_time, s.arrival_time, s.schedule_id,
// //   (d.seat_count - COUNT(t.ticket_id)) as 'availableSeats' from ticket t
// //   inner join train_schedule s on s.schedule_id=t.schedule_id
// //   inner join train_data d on d.train_id=s.train_id
// //   where  d.source =? and d.destination=? and s.runs_onDay=DayName(?)
// //   group by s.schedule_id;
// //   `
// //   db.pool.query(statement, [_from, _to, _journeyDate], (error, result) => {
// //     response.send(utils.createResult(error, result))
// //   })
// // })

router.post('/check_availability', (request, response) => {
  const { _journeyDate, scheduleId } = request.body

  console.log('Checkavalialblity' + _journeyDate + scheduleId)
  const statement = `
  select d.seat_count as totalSeats, COUNT(t.ticket_id) as bookedSeats from ticket t
  inner join train_schedule s on s.schedule_id=t.schedule_id
  inner join train_data d on d.train_id=s.train_id
  where s.schedule_id=? and t.journey_date=?
  group by s.schedule_id;
  `
  //d.train_id,s.schedule_id ,

  db.pool.query(statement, [scheduleId, _journeyDate], (error, data) => {
    const result = {}
    console.log(data)
    console.log(data[0]?.totalSeats) // fetch data only if result of zero is not empty
    console.log(data[0]?.bookedSeats)
    const totalSeats = data[0]?.totalSeats
    const bookedSeats = data[0]?.bookedSeats

    if (data.length === 0) {
      result['status'] = 'first_ticket'
    } else {
      if (bookedSeats <= totalSeats) {
        result.status = 'success'
        const availableSeats = totalSeats - bookedSeats
        //result.data = `Booked seats count = ${bookedSeats} and available seats count = ${availableSeats}`
        result['data'] = {
          totalSeats,
          bookedSeats,
          availableSeats,
        }
      } else {
        result.status = 'error'
        result.error = error
      }
    }
    response.send(result)
  })
})

router.post('/scheduled_train', (request, response) => {
  const { _journeyDate, _from, _to } = request.body

  console.log('schedule' + _journeyDate)
  const statement = `
  select s.schedule_id, d.source , d.destination, d.fare ,s.departue_time, s.arrival_time from train_schedule s
  inner join train_data d on d.train_id=s.train_id
  where d.source =? and d.destination=? and s.runs_onDay=DayName(?);
  `
  db.pool.query(statement, [_from, _to, _journeyDate], (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.post('/available_trains', (request, response) => {
  const { source, destination } = request.body

  const statement = `
  select s.schedule_id, d.source , d.destination, d.fare ,s.departue_time, s.arrival_time, s.runs_onDay from train_schedule s
  inner join train_data d on d.train_id=s.train_id
  where d.source like '%${source}%' and d.destination  like '%${destination}%' ;
  `
  db.pool.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router
