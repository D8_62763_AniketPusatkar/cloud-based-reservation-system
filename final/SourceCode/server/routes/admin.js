const express = require('express')
const cryptoJS = require('crypto-js')
const db = require('../db')
const utils = require('../utils')
const jwt = require('jsonwebtoken')
const config = require('../config')

const router = express.Router()

router.post('/signin', (request, response) => {
  const { email, password } = request.body
  console.log(+password + email)
  // encrpt the password
  //const encryptedPassword = String(cryptoJS.MD5(password))

  // use question marks as placeholders
  // this is used to prevent the sql inejection

  const statement = `
      SELECT admin_id, firstName, lastName, email 
      FROM admin
      WHERE email = ? AND password = ?
  `
  db.pool.query(statement, [email, password], (error, admins) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else {
      if (admins.length === 0) {
        result['status'] = 'error'
        result['error'] = 'admin does not exist'
      } else {
        const admin = admins[0]
        result['status'] = 'success'
        const payload = { admin_id: admin['admin_id'] }
        const token = jwt.sign(payload, config.secret)
        result['data'] = {
          firstName: admin['firstName'],
          lastName: admin['lastName'],
          email: admin['email'],
          token,
        }
      }
    }
    response.send(result)
  })
})

//source | departure_time | destination | arrival_time | distance | fare

router.post('/train_data', (request, response) => {
  const { source, destination, distance, fare, seatCount } = request.body

  const statement = `
  INSERT INTO train_data(source, destination, distance, fare, seat_count) 
  VALUES (?,?,?,?,?);
  `
  db.pool.query(
    statement,
    [source, destination, distance, fare, seatCount],
    (error, result) => {
      response.send(utils.createResult(error, result))
    }
  )
})

router.post('/train_schedule', (request, response) => {
  const { day, departureTime, arrivalTime, trainId } = request.body

  const statement = `
  INSERT INTO train_schedule(runs_onDay, departue_time, arrival_time, train_id) 
  VALUES (?,?,?,?);
  `
  db.pool.query(
    statement,
    [day, departureTime, arrivalTime, trainId],
    (error, result) => {
      response.send(utils.createResult(error, result))
    }
  )
})

router.put('/update_train', (request, response) => {
  const { trainId, fare, seatCount } = request.body

  const statement = `
  update train_data set fare=?, seat_count=? where train_id=?
  `
  db.pool.query(statement, [fare, seatCount, trainId], (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get('/show_trains', (request, response) => {
  const statement = `
  Select * from train_data;
  `
  db.pool.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router
