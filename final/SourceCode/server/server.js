const express = require('express')
const jwt = require('jsonwebtoken')
const utils = require('./utils')
const config = require('./config')
const cors = require('cors')

const app = express()

// enable cors
// for clients to make api calls to this server
app.use(cors())

app.use(express.json())

// middleware to extract the token
app.use((request, response, next) => {
  console.log(request.url)

  if (
    request.url === '/user/signup' ||
    request.url === '/user/signin' ||
    request.url === '/admin/signin'
  ) {
    next()
  } else {
    const token = request.headers['token']
    console.log('token is ' + token)
    if (!token || token.length == 0) {
      response.send(utils.createResult('missing token'))
    } else {
      try {
        const payload = jwt.verify(token, config.secret)

        request.user_id = payload.user_id

        request.admin_id = payload.admin_id
        next()
      } catch (ex) {
        response.send(utils.createResult('invalid token'))
      }
    }
  }
})

const userRouter = require('./routes/user')
const adminRouter = require('./routes/admin')

app.use('/user', userRouter)
app.use('/admin', adminRouter)

app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})
