import axios from 'axios'
import { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import config from '../../config'

const UpdateTrain = () => {
  const [fare, setFare] = useState('')
  const [seatCount, setSeatCount] = useState('')
  const [trainId, setTrainId] = useState(0)
  const [source, setSource] = useState('')
  const [destination, setDestination] = useState('')

  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    const { trainId, source, destination } = location.state
    setTrainId(trainId)
    setSource(source)
    setDestination(destination)
    console.log(source)
    console.log(destination)
  }, [])

  const updateTrain = () => {
    // console.log(day)
    // console.log(departureTime)
    // console.log(arrivalTime)
    // console.log(trainId)

    if (fare.length === 0) {
      toast.error('enter fare')
    } else if (seatCount.length === 0) {
      toast.error('enter seat count')
    } else {
      axios
        .put(
          config.serverURL + '/admin/update_train',
          {
            fare,
            seatCount,
            trainId,
          },
          { headers: { token: sessionStorage['token'] } }
        )
        .then((response) => {
          const result = response.data
          if (result['status'] === 'error') {
            toast.error('Something went wrong ')
          } else {
            toast.success('successfully updated fare and seat-count')
            navigate('/admin_func')
          }
        })
        .catch((error) => {
          console.log('error')
          console.log(error)
        })
    }
  }

  //   schedule_id | runs_onDay | departue_time | arrival_time | train_id

  return (
    <div>
      <div className='container'>
        <div style={{ marginTop: 20 }}>
          <div className='form' style={styles.container}>
            <h3 align='center' style={{ marginBottom: 10 }}>
              Update Train
            </h3>
            <h5 align='center' style={{ marginBottom: 20 }}>
              Updating Fare and SeatCount for : {source}-{destination}
            </h5>
            <div className='row'>
              <div className='col'>
                <div className='mb-3'>
                  <label>Fare</label>
                  <input
                    onKeyUp={(event) => {
                      setFare(event.target.value)
                    }}
                    className='form-control'
                    name='fare'
                    type='number'
                    placeholder='Number'
                  />
                </div>

                <div className='mb-3'>
                  <label>Seat Count</label>
                  <input
                    onKeyUp={(event) => {
                      setSeatCount(event.target.value)
                    }}
                    className='form-control'
                    name='seatCount'
                    type='number'
                    placeholder='Number'
                  />
                </div>
              </div>
            </div>
            <div className='mb-3'>
              <button onClick={updateTrain} style={styles.registerButton}>
                Update Train
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
const styles = {
  container: {
    width: 400,
    height: 400,
    padding: 20,
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#0c6363',
    borderRadius: 10,
    broderWidth: 1,
    borderStyle: 'solid',
    boxShadow: '1px 1px 20px 5px #C9C9C9',
    backgroundColor: '#e3f2fd',
  },
  registerButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: '#0c6363',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 10,
  },
}

export default UpdateTrain
