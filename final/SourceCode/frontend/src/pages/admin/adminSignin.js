import { useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import config from '../../config'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'

const AdminSignin = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const navigate = useNavigate()

  const login = () => {
    console.log(`email = ${email}`)
    console.log(`password = ${password}`)

    if (email.length === 0) {
      toast.error('please enter email')
    } else if (password.length === 0) {
      toast.error('please enter password')
    } else {
      axios
        .post(config.serverURL + '/admin/signin', {
          email,
          password,
        })
        .then((response) => {
          const result = response.data
          if (result['status'] === 'error') {
            toast.error('invalid email or password')
          } else {
            sessionStorage['token'] = result['data']['token']
            sessionStorage['adminName'] = result['data']['firstName']
            toast.success('You have loggedin successfully')
            navigate('/admin_func')
          }
        })
        .catch((error) => {
          console.log('error')
          console.log(error)
        })
    }
  }

  const cancel = () => {
    console.log('cancelling...')

    // reset the values
    setEmail('')
    setPassword('')
  }

  return (
    <div style={{ marginTop: 100 }}>
      <div style={styles.container} className='form'>
        <h3 align='center'>Admin SignIn</h3>
        <div className='mb-3'>
          <label>Email</label>
          <input
            onKeyUp={(event) => {
              setEmail(event.target.value)
            }}
            className='form-control'
            type='email'
            name='email'
            placeholder='abc@gmail.com'
          />
        </div>
        <div className='mb-3'>
          <label>Password</label>
          <input
            onKeyUp={(event) => {
              setPassword(event.target.value)
            }}
            className='form-control'
            type='password'
            name='password'
          />
        </div>
        <div className='mb-3' style={{ marginTop: 20 }}>
          <button onClick={login} style={styles.signinButton}>
            Signin
          </button>
        </div>
      </div>
    </div>
  )
}

const styles = {
  container: {
    width: 400,
    height: 350,
    padding: 20,
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#0c6363',
    borderRadius: 10,
    broderWidth: 1,
    borderStyle: 'solid',
    boxShadow: '1px 1px 20px 5px #C9C9C9',
    backgroundColor: '#e3f2fd',
  },
  signinButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: '#0c6363',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 10,
  },
}

export default AdminSignin
