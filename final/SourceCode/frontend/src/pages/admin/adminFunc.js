import { useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import config from '../../config'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'

const AdminFunctionality = () => {
  const navigate = useNavigate()
  return (
    <div style={{ marginTop: 100 }}>
      <div style={styles.container} className='form'>
        <h3 align='center'>Admin Functionalities</h3>

        <div className='mb-3' style={{ marginTop: 20 }}>
          <button
            className='btn btn-link'
            onClick={() => navigate('/add-train')}
            style={styles.signinButton}>
            Add Train
          </button>
        </div>

        <div className='mb-3' style={{ marginTop: 20 }}>
          <button
            className='btn btn-link'
            onClick={() => navigate('/show-alltrain')}
            style={styles.signinButton}>
            Show All Trains
          </button>
        </div>

        <div className='mb-3' style={{ marginTop: 20 }}>
          <button
            className='btn btn-link'
            onClick={() => navigate('/show-alltrain')}
            style={styles.signinButton}>
            Update And Schedule Trains
          </button>
        </div>

        <div className='mb-3' style={{ marginTop: 20 }}>
          <button
            className='btn btn-link'
            onClick={() => {
              navigate('/user-signin')
              sessionStorage.removeItem('token')
              sessionStorage.removeItem('adminName')
            }}
            style={styles.signinButton}>
            SignOut
          </button>
        </div>
      </div>
    </div>
  )
}

const styles = {
  container: {
    width: 400,
    height: 350,
    padding: 20,
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#0c6363',
    borderRadius: 10,
    broderWidth: 1,
    borderStyle: 'solid',
    boxShadow: '1px 1px 20px 5px #C9C9C9',
    backgroundColor: '#e3f2fd',
  },
  signinButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: '#0c6363',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 10,
  },
}

export default AdminFunctionality
