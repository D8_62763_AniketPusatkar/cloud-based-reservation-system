import axios from 'axios'
import { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import config from '../../config'

const ScheduleTrain = () => {
  const [day, setDay] = useState('Monday')
  const [departureTime, setDepartureTime] = useState('')
  const [arrivalTime, setArrivalTime] = useState('')
  const [trainId, setTrainId] = useState(0)
  const [source, setSource] = useState('')
  const [destination, setDestination] = useState('')

  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    const { trainId, source, destination } = location.state
    setTrainId(trainId)
    setSource(source)
    setDestination(destination)
    console.log(source)
    console.log(destination)
  }, [])

  const scheduleTrain = () => {
    console.log(day)
    console.log(departureTime)
    console.log(arrivalTime)
    console.log(trainId)

    if (departureTime.length === 0) {
      toast.error('enter departure time')
    } else if (arrivalTime.length === 0) {
      toast.error('enter arrival time')
    } else {
      axios
        .post(
          config.serverURL + '/admin/train_schedule',
          {
            day,
            departureTime,
            arrivalTime,
            trainId,
          },
          { headers: { token: sessionStorage['token'] } }
        )
        .then((response) => {
          const result = response.data
          if (result['status'] === 'error') {
            toast.error('Something went wrong ')
          } else {
            toast.success('successfully scheduled a train')
            navigate('/admin_func')
          }
        })
        .catch((error) => {
          console.log('error')
          console.log(error)
        })
    }
  }

  const handleChange = (event) => {
    setDay(event.target.value)
  }
  //   schedule_id | runs_onDay | departue_time | arrival_time | train_id

  return (
    <div>
      <div className='container'>
        <div style={{ marginTop: 20 }}>
          <div className='form' style={styles.container}>
            <h3 align='center' style={{ marginBottom: 10 }}>
              Schedule Train
            </h3>
            <h5 align='center' style={{ marginBottom: 20 }}>
              Train is being scheduled for route: {source}-{destination}
            </h5>
            <div className='row'>
              <div className='col'>
                <div className='mb-3'>
                  <label>
                    Select Day
                    <select
                      value={day}
                      onChange={handleChange}
                      style={{ marginLeft: 10 }}>
                      <option value='Monday'>Monday</option>
                      <option value='Tuesday'>Tuesday</option>
                      <option value='Wednesday'>Wednesday</option>
                      <option value='Thursday'>Thursday</option>
                      <option value='Friday'>Friday</option>
                      <option value='Saturday'>Saturday</option>
                      <option value='Sunday'>Sunday</option>
                    </select>
                  </label>
                </div>

                <div className='mb-3'>
                  <label>Departure Time</label>
                  <input
                    onKeyUp={(event) => {
                      setDepartureTime(event.target.value)
                    }}
                    className='form-control'
                    name='departue_time'
                    type='text'
                    placeholder='00-23:00-59:00-59'
                  />
                </div>

                <div className='mb-3'>
                  <label>Arrival Time</label>
                  <input
                    onKeyUp={(event) => {
                      setArrivalTime(event.target.value)
                    }}
                    className='form-control'
                    name='arrival_time'
                    type='text'
                    placeholder='00-23:00-59:00-59'
                  />
                </div>
              </div>
            </div>
            <div className='mb-3'>
              <button onClick={scheduleTrain} style={styles.registerButton}>
                Schedule Train
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
const styles = {
  container: {
    width: 400,
    height: 450,
    padding: 20,
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#0c6363',
    borderRadius: 10,
    broderWidth: 1,
    borderStyle: 'solid',
    boxShadow: '1px 1px 20px 5px #C9C9C9',
    backgroundColor: '#e3f2fd',
  },
  registerButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: '#0c6363',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 10,
  },
}

export default ScheduleTrain
