import axios from 'axios'
import config from '../../config'
import { toast } from 'react-toastify'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

const ShowAllTrains = () => {
  const [trains, setTrains] = useState([])

  const navigate = useNavigate()

  useEffect(() => {
    getTrains()
  }, [])

  const getTrains = () => {
    axios
      .get(config.serverURL + '/admin/show_trains', {
        headers: { token: sessionStorage['token'] },
      })
      .then((response) => {
        const result = response.data

        if (result['status'] === 'success') {
          console.log(result)
          // set the homes to the state member
          setTrains(result['data'])
        } else {
          toast.error(result['error'])
        }
      })
  }

  // edit my home
  const scheduleTrain = (id, source, destination) => {
    // pass the home id which you want to edit
    navigate('/schedule-train', {
      state: { trainId: id, source: source, destination: destination },
    })
  }

  const updateTrain = (id, source, destination) => {
    // pass the home id which you want to edit
    navigate('/update-train', {
      state: { trainId: id, source: source, destination: destination },
    })
  }

  return (
    <div className='container' style={{ backgroundColor: '#e3f2fd' }}>
      <h3 style={styles.h3}>All Routes</h3>
      <table className='table table-striped'>
        <thead>
          <tr align='center'>
            <th>Route No</th>
            <th>Source</th>
            <th>Destination</th>
            <th>Distance</th>
            <th>Fare</th>
            <th>Seat Count</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {trains.map((train) => {
            return (
              <tr align='center'>
                {/* train_id | source | destination | distance | fare | seat_count */}
                <td>{train.train_id}</td>
                <td>{train.source}</td>
                <td>{train.destination}</td>
                <td>{train.distance}</td>
                <td>{train.fare}</td>
                <td>{train.seat_count}</td>
                <td>
                  <button
                    onClick={() =>
                      scheduleTrain(
                        train.train_id,
                        train.source,
                        train.destination
                      )
                    }
                    style={styles.button}
                    className='btn-sm btn btn-warning'>
                    Schedule
                  </button>
                  <button
                    onClick={() =>
                      updateTrain(
                        train.train_id,
                        train.source,
                        train.destination
                      )
                    }
                    style={styles.button}
                    className='btn-sm btn btn-primary'>
                    Update
                  </button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

const styles = {
  h3: {
    textAlign: 'center',
    margin: 20,
  },
  button: {
    marginRight: 10,
  },
}

export default ShowAllTrains
