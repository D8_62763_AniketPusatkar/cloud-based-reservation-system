import axios from 'axios'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import config from '../../config'

const AddTrain = () => {
  const [source, setSource] = useState('')
  const [destination, setDestination] = useState('')
  const [distance, setDistance] = useState(0)
  const [fare, setFare] = useState(0)
  const [seatCount, setSeatCount] = useState(0)

  const navigate = useNavigate()

  const AddTrain = () => {
    if (source.length === 0) {
      toast.error('enter source')
    } else if (destination.length === 0) {
      toast.error('enter destination')
    } else if (distance.length === 0) {
      toast.error('enter distance')
    } else if (fare.length === 0) {
      toast.error('enter fare')
    } else if (seatCount.length === 0) {
      toast.error('enter seatCount')
    } else {
      axios
        .post(
          config.serverURL + '/admin/train_data',
          {
            source,
            destination,
            distance,
            fare,
            seatCount,
          },
          { headers: { token: sessionStorage['token'] } }
        )
        .then((response) => {
          const result = response.data
          if (result['status'] === 'error') {
            toast.error('Something went wrong ')
          } else {
            toast.success('successfully added new route')
            navigate('/admin_func')
          }
        })
        .catch((error) => {
          console.log('error')
          console.log(error)
        })
    }
  }
  return (
    <div>
      <div className='container'>
        <div style={{ marginTop: 20 }}>
          <div className='form' style={styles.container}>
            <h3 align='center'>Add Train</h3>
            <div className='row'>
              <div className='col'>
                <div className='mb-3'>
                  {' '}
                  <label>Source</label>
                  <input
                    onKeyUp={(event) => {
                      setSource(event.target.value)
                    }}
                    className='form-control'
                    name='source'
                    type='text'
                    placeholder='Example : Pune'
                  />
                </div>

                <div className='mb-3'>
                  <label>Destination</label>
                  <input
                    onKeyUp={(event) => {
                      setDestination(event.target.value)
                    }}
                    className='form-control'
                    name='destination'
                    type='text'
                    placeholder='Example : Mumbai'
                  />
                </div>

                <div className='mb-3'>
                  <label>distance</label>
                  <input
                    onKeyUp={(event) => {
                      setDistance(event.target.value)
                    }}
                    className='form-control'
                    name='distance'
                    type='number'
                    placeholder='Number'
                  />
                </div>
                <div className='mb-3'>
                  <label>Fare</label>
                  <input
                    onKeyUp={(event) => {
                      setFare(event.target.value)
                    }}
                    className='form-control'
                    name='fare'
                    type='number'
                    placeholder='Number'
                  />
                </div>
                <div className='mb-3'>
                  <label>Seat Count</label>
                  <input
                    onKeyUp={(event) => {
                      setSeatCount(event.target.value)
                    }}
                    className='form-control'
                    name='seatCount'
                    type='number'
                    placeholder='Number'
                  />
                </div>
              </div>
            </div>
            <div className='mb-3'>
              <button onClick={AddTrain} style={styles.registerButton}>
                Add Train
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
const styles = {
  container: {
    width: 400,
    height: 600,
    padding: 20,
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#0c6363',
    borderRadius: 10,
    broderWidth: 1,
    borderStyle: 'solid',
    boxShadow: '1px 1px 20px 5px #C9C9C9',
    backgroundColor: '#e3f2fd',
  },
  registerButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: '#0c6363',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 10,
  },
}

export default AddTrain
