import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { signin } from '../slices/authSlice'
import backgroundImage from '../train.jpeg'

const Home = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate
  useEffect(() => {
    if (sessionStorage['token'] && sessionStorage['token'].length > 0) {
      const user = {
        token: sessionStorage['token'],
        firstName: sessionStorage['username'],
      }
      dispatch(signin(user))
    }
  }, [])

  return (
    <div style={{ backgroundImage: `url(${backgroundImage})` }}>
      <h1 align='center'>Welcome To Online Railway Reservation System</h1>
    </div>
  )
}

export default Home
