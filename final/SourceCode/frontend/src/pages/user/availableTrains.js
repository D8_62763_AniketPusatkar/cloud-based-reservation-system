import axios from 'axios'
import config from '../../config'
import { toast } from 'react-toastify'
import { useEffect, useState } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'

const AvailableTrains = () => {
  const [shedTrains, setShedTrains] = useState([])
  const [_journeyDate, setDate] = useState('')
  const [_from, setFrom] = useState('')
  const [_to, setTo] = useState('')

  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    const { from, to, journey_date } = location.state
    setFrom(from)
    setTo(to)
    setDate(journey_date)
  }, [])

  useEffect(() => {
    if (_from && _journeyDate && _to) {
      scheduledTrains()
    }
  }, [_from, _journeyDate, _to])

  const scheduledTrains = () => {
    console.log('in handler', _from)
    console.log(_journeyDate)
    console.log(_to)

    axios
      .post(
        config.serverURL + '/user/scheduled_train',
        {
          _journeyDate,
          _from,
          _to,
        },
        { headers: { token: sessionStorage['token'] } }
      )
      .then((response) => {
        const result = response.data
        if (result['status'] === 'error') {
          toast.error('Something went wrong ')
        } else {
          const railResult = result.data
          console.log(railResult)
          setShedTrains(railResult)
        }
      })
      .catch((error) => {
        console.log('error')
        console.log(error)
      })
  }

  const checkAvailability = (
    scheduleId,
    source,
    destination,
    arrivalTime,
    departureTime,
    fare
  ) => {
    console.log(scheduleId)
    console.log(_journeyDate)
    axios
      .post(
        config.serverURL + '/user/check_availability',
        {
          _journeyDate,
          scheduleId,
        },
        { headers: { token: sessionStorage['token'] } }
      )
      .then((response) => {
        const result = response.data
        if (result['status'] === 'error') {
          toast.error('Something went wrong')

          if (!sessionStorage['token']) {
            navigate('/user-signin')
          }
        } else if (result['status'] === 'first_ticket') {
          toast.success(`All seats are available`)
          navigate('/booked_ticket', {
            state: {
              scheduleId,
              _journeyDate,
              source,
              destination,
              arrivalTime,
              departureTime,
              fare,
            },
          })
        } else if (result.data.availableSeats === 0) {
          toast.error('All seats are occupied')
        } else {
          console.log(result)
          const availableSeats = result.data.availableSeats
          toast.success(`Avaialable seats are ${availableSeats}`)
          navigate('/booked_ticket', {
            state: {
              scheduleId,
              _journeyDate,
              source,
              destination,
              arrivalTime,
              departureTime,
              fare,
            },
          })
        }
      })
      .catch((error) => {
        console.log('error')
        console.log(error)
      })
  }

  return (
    <div className='container' style={{ backgroundColor: '#e3f2fd' }}>
      <h3 style={styles.h3}>
        Available Trains for the selected date {_journeyDate}{' '}
      </h3>
      <table className='table table-striped'>
        <thead>
          <tr align='center'>
            <th>Source</th>
            <th>Destination</th>
            <th>Departure Time</th>
            <th>Arrival Time</th>
            <th>Fare</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {shedTrains.map((shedTrain) => {
            return (
              <tr align='center' key={shedTrain.schedule_id}>
                {/* train_id | source | destination | distance | fare | seat_count */}
                <td>{shedTrain.source}</td>
                <td>{shedTrain.destination}</td>
                <td>{shedTrain.departue_time}</td>
                <td>{shedTrain.arrival_time}</td>
                <td>{shedTrain.fare}</td>
                <td>
                  <button
                    onClick={() =>
                      checkAvailability(
                        shedTrain.schedule_id,
                        shedTrain.source,
                        shedTrain.destination,
                        shedTrain.departue_time,
                        shedTrain.arrival_time,
                        shedTrain.fare
                      )
                    }
                    style={styles.button}
                    className='btn-sm btn btn-primary'>
                    Book Ticket
                  </button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

const styles = {
  h3: {
    textAlign: 'center',
    margin: 20,
  },
  button: {
    marginRight: 10,
  },
}

export default AvailableTrains
