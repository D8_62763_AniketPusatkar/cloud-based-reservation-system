import axios from 'axios'
import config from '../../config'
import { toast } from 'react-toastify'
import { useEffect, useState } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'

const BookedTicketInfo = () => {
  const [_scheduleId, setScheduleId] = useState(0)
  const [journey_date, setDate] = useState('')
  const [from, setSource] = useState('')
  const [to, setDestination] = useState('')
  const [_arrivalTime, setArrivalTime] = useState('')
  const [_departureTime, setDepartureTime] = useState('')
  const [_fare, setFare] = useState(0)
  const [user, setUser] = useState()
  const [_firstName, setFirstName] = useState('')
  const [_lastName, setLastName] = useState('')
  const [city, setCity] = useState('')
  const [gender, setGender] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')

  const location = useLocation()
  const navigate = useNavigate()

  const [show, setShow] = useState(false)
  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  useEffect(() => {
    const {
      scheduleId,
      _journeyDate,
      source,
      destination,
      arrivalTime,
      departureTime,
      fare,
    } = location.state
    setScheduleId(scheduleId)
    setDate(_journeyDate)
    setSource(source)
    setDestination(destination)
    setArrivalTime(arrivalTime)
    setDepartureTime(departureTime)
    setFare(fare)
  }, [])

  useEffect(() => {
    if (
      from &&
      to &&
      _arrivalTime &&
      _departureTime &&
      _fare &&
      _scheduleId &&
      journey_date
    ) {
      userDetails()
    }
  }, [from, to, _arrivalTime, _departureTime, _fare, _scheduleId, journey_date])

  const userDetails = () => {
    axios
      .get(
        config.serverURL + '/user/userDetails',

        { headers: { token: sessionStorage['token'] } }
      )
      .then((response) => {
        const result = response.data
        if (result['status'] === 'error') {
          toast.error('Something went wrong')
        } else {
          console.log(result)
          console.log(result.data[0].firstName)
          console.log(result.data[0].lastName)

          setFirstName(result.data[0].firstName)
          setLastName(result.data[0].lastName)
          setCity(result.data[0].city)
          setEmail(result.data[0].email)
          setGender(result.data[0].gender)
          setPhone(result.data[0].phone)
        }
      })
      .catch((error) => {
        console.log('error')
        console.log(error)
      })
  }

  useEffect(() => {
    if (_firstName && _lastName && email && city && gender && phone) {
      handleShow()
    }
  }, [_firstName, _lastName, email, city, gender, phone])

  //   useEffect(() => {
  //     if (_firstName&&_lastName) {
  //       handleShow()
  //     }
  //   }, [_firat])

  const bookTicket = () => {
    axios
      .post(
        config.serverURL + '/user/book_ticket',
        {
          journey_date,
          _scheduleId,
        },
        { headers: { token: sessionStorage['token'] } }
      )
      .then((response) => {
        const result = response.data
        if (result['status'] === 'error') {
          toast.error('Something went wrong')
        } else {
          toast.success('Ticket booked successfully')
          navigate('/')
        }
      })
      .catch((error) => {
        console.log('error')
        console.log(error)
      })
  }

  const goBack = () => {
    navigate('/available_trains', { state: { journey_date, from, to } })
  }

  return (
    <Modal
      size='sm'
      show={show}
      onHide={handleClose}
      aria-labelledby='contained-modal-title-vcenter'
      backdrop='static'
      keyboard={false}
      centered>
      <Modal.Header>
        <Modal.Title id='contained-modal-title-vcenter'>
          Ticket Details
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h6>
          <b>
            <i>Name</i>
          </b>
          : "{_firstName} {_lastName}"
        </h6>
        <h6>
          <b>
            <i>Source</i>
          </b>
          : "{from}"
        </h6>
        <h6>
          <b>
            <i>Destination</i>
          </b>
          : "{to}"
        </h6>
        <h6>
          <b>
            <i>Departure Time</i>
          </b>
          : "{_departureTime} "
        </h6>
        <h6>
          <b>
            <i>Arrival Time</i>
          </b>
          : "{_arrivalTime}"
        </h6>
        <h6>
          <b>
            <i>Fare </i>
          </b>
          : {_fare} Rs.
        </h6>
        <h6>
          <b>
            <i>Journey Date</i>
          </b>
          : "{journey_date}"
        </h6>
        <h6>
          <b>
            <i>Email</i>
          </b>
          : "{email}"
        </h6>
        <h6>
          <b>
            <i>City</i>
          </b>
          : "{city}"
        </h6>
        <h6>
          <b>
            <i>Gender</i>
          </b>
          : "{gender}"
        </h6>
      </Modal.Body>
      <Modal.Footer>
        <Button className='btn btn-success' onClick={bookTicket}>
          Book Ticket
        </Button>
        <Button onClick={goBack}>Go back</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default BookedTicketInfo
