import axios from 'axios'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import config from '../../config'
import moment from 'moment'

const CancelById = () => {
  const [ticketId, setTicketId] = useState('')

  const navigate = useNavigate()

  const ticketCancel = () => {
    if (ticketId.length === 0) {
      toast.error('Enter ticket id')
    } else {
      navigate('/delete_ticketById', {
        state: { ticketId },
      })
    }
  }
  return (
    <div>
      <div className='container'>
        <div style={{ marginTop: 100 }}>
          <div className='form' style={styles.container}>
            <h3 align='center'>Cancel Ticket</h3>
            <div className='row'>
              <div className='col'>
                <div className='mb-3'>
                  {' '}
                  <label>Ticket Id</label>
                  <input
                    onKeyUp={(event) => {
                      setTicketId(event.target.value)
                    }}
                    className='form-control'
                    name='ticketId'
                    type='number'
                    placeholder='number'
                  />
                </div>
              </div>
            </div>
            <div className='mb-3'>
              <button onClick={ticketCancel} style={styles.registerButton}>
                Cancel Ticket
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
const styles = {
  container: {
    width: 400,
    height: 250,
    padding: 20,
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#0c6363',
    borderRadius: 10,
    broderWidth: 1,
    borderStyle: 'solid',
    boxShadow: '1px 1px 20px 5px #C9C9C9',
    backgroundColor: '#e3f2fd',
  },
  registerButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: '#0c6363',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 10,
  },
}

export default CancelById
