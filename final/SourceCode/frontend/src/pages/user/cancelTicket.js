import axios from 'axios'
import config from '../../config'
import { toast } from 'react-toastify'
import { useEffect, useState } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'

const CancelTicket = () => {
  const [_ticketId, setTicketId] = useState(0)

  const location = useLocation()
  const navigate = useNavigate()

  const [show, setShow] = useState(false)
  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  useEffect(() => {
    const { ticketId } = location.state
    setTicketId(ticketId)
  }, [])

  useEffect(() => {
    if (_ticketId) {
      handleShow()
    }
  }, [_ticketId])

  const deleteTicket = () => {
    axios
      .delete(
        config.serverURL + '/user/cancel_ticket' + _ticketId,

        { headers: { token: sessionStorage['token'] } }
      )
      .then((response) => {
        const result = response.data
        if (result['status'] === 'error') {
          toast.error('Something went wrong')
        } else {
          navigate('/show_allTickets')
        }
      })
      .catch((error) => {
        console.log('error')
        console.log(error)
      })
  }
  const goBack = () => {
    navigate('/show_allTickets')
  }
  return (
    <Modal
      size='md'
      show={show}
      onHide={handleClose}
      aria-labelledby='contained-modal-title-vcenter'
      backdrop='static'
      keyboard={false}
      centered>
      <Modal.Header closeButton>
        <Modal.Title id='contained-modal-title-vcenter'>
          Ticket Cancellation
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>
          Are you sure ? you want to cancel the ticket with ticket id '
          {_ticketId}'
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button className='btn btn-danger' onClick={deleteTicket}>
          Confirm Cancel Ticket
        </Button>
        <Button onClick={goBack}>Go back</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default CancelTicket
