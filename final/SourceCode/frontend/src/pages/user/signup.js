import { useState } from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import config from '../../config'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

const Signup = () => {
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [birthDate, setBirthdate] = useState('')
  const [city, setCity] = useState('')
  const [pincode, setPincode] = useState('')
  const [gender, setGender] = useState('')

  const navigate = useNavigate()

  const Register = () => {
    console.log(`email = ${email}`)
    console.log(`password = ${password}`)
    console.log(`name = ${firstName}`)
    console.log(`dob = ${birthDate}`)
    console.log(`gender = ${gender}`)

    if (firstName.length === 0) {
      toast.error('please enter name')
    } else if (lastName.length === 0) {
      toast.error('please enter last name')
    } else if (email.length === 0) {
      toast.error('please enter email')
    } else if (phone.length === 0) {
      toast.error('please enter phone number')
    } else if (password.length === 0) {
      toast.error('please enter password')
    } else if (confirmPassword.length === 0) {
      toast.error('please confirm password')
    } else if (password !== confirmPassword) {
      toast.error('password does not match')
    } else if (birthDate.length === 0) {
      toast.error('please enter date of birth')
    } else if (city.length === 0) {
      toast.error('please enter city')
    } else if (pincode.length === 0) {
      toast.error('please enter pincode')
    } else if (gender.length === 0) {
      toast.error('please select gender')
    } else {
      // make the API call to check if user exists
      axios
        .post(config.serverURL + '/user/signup', {
          firstName,
          lastName,
          email,
          password,
          phone,
          birthDate,
          city,
          pincode,
          gender,
        })
        .then((response) => {
          // get the data returned by server
          const result = response.data

          // check if user's authentication is successfull
          if (result['status'] === 'error') {
            toast.error('Something went wrong ')
          } else {
            toast.success('successfully registered a new user')

            // navigate to the singin page
            navigate('/user-signin')
          }
        })
        .catch((error) => {
          console.log('error')
          console.log(error)
        })
    }
  }

  return (
    <div className='container'>
      <div style={{ marginTop: 20 }}>
        <div className='form' style={styles.container}>
          <h3 align='center'>User SignUp</h3>
          <div className='row'>
            <div className='col'>
              <div className='mb-3'>
                {' '}
                <label>First Name</label>
                <input
                  onKeyUp={(event) => {
                    setFirstName(event.target.value)
                  }}
                  className='form-control'
                  name='firstName'
                  type='text'
                  placeholder='Example : Sandip'
                />
              </div>

              <div className='mb-3'>
                <label>Last Name</label>
                <input
                  onKeyUp={(event) => {
                    setLastName(event.target.value)
                  }}
                  className='form-control'
                  name='lastName'
                  type='text'
                  placeholder='Example : Patil'
                />
              </div>

              <div className='mb-3'>
                <label>Email</label>
                <input
                  onKeyUp={(event) => {
                    setEmail(event.target.value)
                  }}
                  className='form-control'
                  name='email'
                  type='email'
                  placeholder='abc@gmail.com'
                />
              </div>
              <div className='mb-3'>
                <label>Password</label>
                <input
                  onKeyUp={(event) => {
                    setPassword(event.target.value)
                  }}
                  className='form-control'
                  name='password'
                  type='password'
                />
              </div>
              <div className='mb-3'>
                <label>Confirm Password</label>
                <input
                  onKeyUp={(event) => {
                    setConfirmPassword(event.target.value)
                  }}
                  className='form-control'
                  name='confirmPassword'
                  type='password'
                />
              </div>
            </div>
            <div className='col'>
              {' '}
              <div className='mb-3'>
                <label>Mobile Number</label>
                <input
                  onKeyUp={(event) => {
                    setPhone(event.target.value)
                  }}
                  className='form-control'
                  name='phone'
                  type='number'
                  placeholder='number'
                />
              </div>
              <div className='mb-3'>
                <label>Date of Birth</label>
                <input
                  type='date'
                  onChange={(e) => setBirthdate(e.target.value)}
                  style={{ marginLeft: 10 }}
                />
              </div>
              <div className='mb-3'>
                <label>City</label>
                <input
                  onKeyUp={(event) => {
                    setCity(event.target.value)
                  }}
                  className='form-control'
                  name='city'
                  type='text'
                  placeholder='Text'
                />
              </div>
              <div className='mb-3'>
                <label>Pin Code</label>
                <input
                  onKeyUp={(event) => {
                    setPincode(event.target.value)
                  }}
                  className='form-control'
                  name='pincode'
                  type='number'
                  placeholder='number'
                />
              </div>
              <div className='mb-3'>
                <label>Select Gender :</label>
                <input
                  type='radio'
                  id='male'
                  onChange={(event) => {
                    setGender(event.target.value)
                  }}
                  name='gender'
                  value='M'
                  style={{ marginLeft: 10 }}
                />
                {'  '}
                <label for='male'>Male</label>
                {'  '}
                <input
                  type='radio'
                  id='female'
                  onChange={(event) => {
                    setGender(event.target.value)
                  }}
                  name='gender'
                  value='F'
                  style={{ marginLeft: 10 }}
                />
                {'  '}
                <label for='female'>Female</label>
                {'  '}
                <input
                  type='radio'
                  id='other'
                  onChange={(event) => {
                    setGender(event.target.value)
                  }}
                  name='gender'
                  value='O'
                  style={{ marginLeft: 10 }}
                />
                {'  '}
                <label for='other'> Other</label>
              </div>
            </div>
          </div>
          <div className='mb-3'>
            <button onClick={Register} style={styles.registerButton}>
              Register
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

const styles = {
  container: {
    width: 900,
    height: 600,
    padding: 20,
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#0c6363',
    borderRadius: 10,
    broderWidth: 1,
    borderStyle: 'solid',
    boxShadow: '1px 1px 20px 5px #C9C9C9',
    backgroundColor: '#e3f2fd',
  },
  registerButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: '#0c6363',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 10,
  },
}

export default Signup
