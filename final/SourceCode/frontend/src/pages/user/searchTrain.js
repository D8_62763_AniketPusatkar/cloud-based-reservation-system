import axios from 'axios'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import config from '../../config'
import moment from 'moment'

const SearchTrain = () => {
  const [source, setSource] = useState('')
  const [destination, setDestination] = useState('')

  const navigate = useNavigate()
  useEffect(() => {
    if (!sessionStorage['token']) {
      navigate('/user-signin')
    }
  }, [])

  const search = () => {
    if (source.length === 0) {
      toast.error('enter source')
    } else if (destination.length === 0) {
      toast.error('enter destination')
    } else {
      navigate('/route_search', {
        state: { source, destination },
      })
    }
  }
  return (
    <div>
      <div className='container'>
        <div style={{ marginTop: 100 }}>
          <div className='form' style={styles.container}>
            <h3 align='center'>Search Train</h3>
            <div className='row'>
              <div className='col'>
                <div className='mb-3'>
                  {' '}
                  <label>Source</label>
                  <input
                    onKeyUp={(event) => {
                      setSource(event.target.value)
                    }}
                    className='form-control'
                    name='source'
                    type='text'
                    placeholder='Text'
                  />
                </div>

                <div className='mb-3'>
                  <label>Destination</label>
                  <input
                    onKeyUp={(event) => {
                      setDestination(event.target.value)
                    }}
                    className='form-control'
                    name='destination'
                    type='text'
                    placeholder='Text'
                  />
                </div>
              </div>
            </div>
            <div className='mb-3'>
              <button onClick={search} style={styles.registerButton}>
                Available Trains
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
const styles = {
  container: {
    width: 400,
    height: 350,
    padding: 20,
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#0c6363',
    borderRadius: 10,
    broderWidth: 1,
    borderStyle: 'solid',
    boxShadow: '1px 1px 20px 5px #C9C9C9',
    backgroundColor: '#e3f2fd',
  },
  registerButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: '#0c6363',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 10,
  },
}

export default SearchTrain
