import axios from 'axios'
import config from '../../config'
import { toast } from 'react-toastify'
import { useEffect, useState } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'

const TrainRoutes = () => {
  const [matchingTrains, setMatchingTrains] = useState([])
  const [source, setSource] = useState('')
  const [destination, setDestination] = useState('')

  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    const { source, destination } = location.state
    setSource(source)
    setDestination(destination)
  }, [])

  useEffect(() => {
    if (source && destination) {
      findTrains()
    }
  }, [source, destination])

  const findTrains = () => {
    axios
      .post(
        config.serverURL + '/user/available_trains',
        {
          source,
          destination,
        },
        { headers: { token: sessionStorage['token'] } }
      )
      .then((response) => {
        const result = response.data
        if (result['status'] === 'error') {
          toast.error('Something went wrong ')
        } else {
          const railResult = result.data
          console.log(railResult)
          setMatchingTrains(railResult)
        }
      })
      .catch((error) => {
        console.log('error')
        console.log(error)
      })
  }

  const goBack = () => {
    navigate('/')
  }

  const bookTicket = () => {
    navigate('/scheduled_train')
  }

  return (
    <div className='container' style={{ backgroundColor: '#e3f2fd' }}>
      <h3 style={styles.h3}>Available Trains For the Route</h3>
      <table className='table table-striped'>
        <thead>
          <tr align='center'>
            <th>Source</th>
            <th>Destination</th>
            <th>Day</th>
            <th>Departure Time</th>
            <th>Arrival Time</th>
            <th>Fare</th>
          </tr>
        </thead>
        <tbody>
          {matchingTrains.map((matchingTrain) => {
            return (
              <tr align='center' key={matchingTrain.schedule_id}>
                {/* train_id | source | destination | distance | fare | seat_count */}
                <td>{matchingTrain.source}</td>
                <td>{matchingTrain.destination}</td>
                <td>{matchingTrain.runs_onDay}</td>
                <td>{matchingTrain.departue_time}</td>
                <td>{matchingTrain.arrival_time}</td>
                <td>{matchingTrain.fare}</td>
              </tr>
            )
          })}

          <button
            onClick={goBack}
            style={styles.button}
            className='btn-sm btn btn-primary'>
            Go back
          </button>
        </tbody>
      </table>
    </div>
  )
}

const styles = {
  h3: {
    textAlign: 'center',
    margin: 20,
  },
  button: {
    marginTop: 20,
    marginBottom: 20,
  },
}

export default TrainRoutes
