import axios from 'axios'
import config from '../../config'
import { toast } from 'react-toastify'
import { useEffect, useState } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import moment from 'moment'

const ShowAllTckets = () => {
  const [allTickets, setAllTickets] = useState([])

  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    getAllTickets()
  }, [])

  const getAllTickets = () => {
    axios
      .get(
        config.serverURL + '/user/show_allTicket',

        { headers: { token: sessionStorage['token'] } }
      )
      .then((response) => {
        const result = response.data
        if (result['status'] === 'error') {
          toast.error('Something went wrong ')
          if (!sessionStorage['token']) {
            navigate('/user-signin')
          }
        } else {
          const tickets = result.data
          console.log(tickets)
          setAllTickets(tickets)
        }
      })
      .catch((error) => {
        console.log('error')
        console.log(error)
      })
  }

  const cancelTicket = (ticketId) => {
    navigate('/cancel_ticket', { state: { ticketId } })
  }

  return (
    <div className='container' style={{ backgroundColor: '#e3f2fd' }}>
      <h3 style={styles.h3}>
        List of Tickets Booked by "{allTickets[0]?.firstName}{' '}
        {allTickets[0]?.lastName}"
      </h3>
      <table className='table table-striped'>
        <thead>
          <tr align='center'>
            <th>Ticket Id</th>
            <th>Source</th>
            <th>Destination</th>
            <th>Departure Time</th>
            <th>Arrival Time</th>
            <th>Journey Date</th>
            <th>Fare</th>
            <th>Current date</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {allTickets.map((ticket) => {
            return (
              <tr align='center'>
                {/* train_id | source | destination | distance | fare | seat_count */}
                <td>{ticket.ticket_id}</td>

                <td>{ticket.source}</td>
                <td>{ticket.destination}</td>
                <td>{ticket.departue_time}</td>
                <td>{ticket.arrival_time}</td>
                <td>{moment(ticket.journey_date).format('YYYY/MM/DD')}</td>
                <td>{ticket.fare}</td>
                <td>{moment(Date.now()).format('YYYY/MM/DD')}</td>
                <td>
                  {moment(
                    moment(ticket.journey_date).format('YYYY/MM/DD')
                  ).isAfter(moment(Date.now()).format('YYYY/MM/DD')) && (
                    <button
                      onClick={() => {
                        cancelTicket(ticket.ticket_id)
                      }}
                      style={styles.button}
                      className='btn-sm btn btn-primary'>
                      Cancel Ticket
                    </button>
                  )}
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

const styles = {
  h3: {
    textAlign: 'center',
    margin: 20,
  },
  button: {
    marginRight: 10,
  },
}

export default ShowAllTckets
