import axios from 'axios'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import config from '../../config'
import moment from 'moment'
import { signin } from '../../slices/authSlice'
import { useDispatch } from 'react-redux'

const BookTicket = () => {
  const [from, setFrom] = useState('')
  const [to, setTo] = useState('')
  const [journey_date, setDate] = useState('')

  const navigate = useNavigate()
  const dispatch = useDispatch()

  useEffect(() => {
    if (sessionStorage['token'] && sessionStorage['token'].length > 0) {
      const user = {
        token: sessionStorage['token'],
        firstName: sessionStorage['username'],
      }
      dispatch(signin(user))
    }
    if (!sessionStorage['token']) {
      navigate('/user-signin')
    }
  }, [])

  const scheduledTrains = (journey_date, from, to) => {
    if (from.length === 0) {
      toast.error('enter source')
    } else if (to.length === 0) {
      toast.error('enter destination')
    } else if (journey_date.length === 0) {
      toast.error('enter date')
    } else if (
      !moment(moment(journey_date).format('YYYY/MM/DD')).isAfter(
        moment(Date.now()).format('YYYY/MM/DD')
      )
    ) {
      toast.error('enter valid date for booking ticket')
    } else {
      navigate('/available_trains', {
        state: { journey_date: journey_date, from: from, to: to },
      })
    }
  }
  return (
    <div>
      <div className='container'>
        <div style={{ marginTop: 100 }}>
          <div className='form' style={styles.container}>
            <h3 align='center'>Book Ticket</h3>
            <div className='row'>
              <div className='col'>
                <div className='mb-3'>
                  {' '}
                  <label>Source</label>
                  <input
                    onKeyUp={(event) => {
                      setFrom(event.target.value)
                    }}
                    className='form-control'
                    name='source'
                    type='text'
                    placeholder='Text'
                  />
                </div>

                <div className='mb-3'>
                  <label>Destination</label>
                  <input
                    onKeyUp={(event) => {
                      setTo(event.target.value)
                    }}
                    className='form-control'
                    name='destination'
                    type='text'
                    placeholder='Text'
                  />
                </div>

                <div className='mb-3'>
                  <label>Date</label>
                  <input
                    type='date'
                    onChange={(e) => setDate(e.target.value)}
                    style={{ marginLeft: 10 }}
                    name='journey_date'
                  />
                </div>
              </div>
            </div>
            <div className='mb-3'>
              <button
                onClick={() => scheduledTrains(journey_date, from, to)}
                style={styles.registerButton}>
                Available Trains
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
const styles = {
  container: {
    width: 400,
    height: 400,
    padding: 20,
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#0c6363',
    borderRadius: 10,
    broderWidth: 1,
    borderStyle: 'solid',
    boxShadow: '1px 1px 20px 5px #C9C9C9',
    backgroundColor: '#e3f2fd',
  },
  registerButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: '#0c6363',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 10,
  },
}

export default BookTicket
