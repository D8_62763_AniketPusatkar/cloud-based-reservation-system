import { BrowserRouter, Routes, Route, Link } from 'react-router-dom'
import Navbar from './components/navbar'
import AddTrain from './pages/admin/addTrain'
import AdminSignin from './pages/admin/adminSignin'
import RemoveTrain from './pages/admin/removeTrain'
import Home from './pages/home'
import Signup from './pages/user/signup'
import UpdateProfile from './pages/user/updateProfile'
import UserSignin from './pages/user/userSignin'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import ScheduleTrain from './pages/admin/scheduleTrain'
import ShowAllTrains from './pages/admin/showAllTrains'
import UpdateTrain from './pages/admin/updateTrain'
import BookTicket from './pages/user/bookTicket'
import AvailableTrains from './pages/user/availableTrains'
import BookedTicketInfo from './pages/user/bookedTicketInfo'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import ShowAllTckets from './pages/user/showAllTckets'
import CancelTicket from './pages/user/cancelTicket'
import AdminFunctionality from './pages/admin/adminFunc'
import SearchTrain from './pages/user/searchTrain'
import TrainRoutes from './pages/user/trainRoutes'
import CancelById from './pages/user/cancelById'
import DeleteTicketById from './pages/user/deleteTicketById'
import { signin } from './slices/authSlice'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'

function App() {
  const signinStatus = useSelector((state) => state.authSlice.status)
  console.log(signinStatus)
  const dispatch = useDispatch()

  useEffect(() => {
    console.log('inside useEffect of app.js')
    if (sessionStorage['token'] && sessionStorage['token'].length > 0) {
      const user = {
        token: sessionStorage['token'],
        firstName: sessionStorage['username'],
      }
      dispatch(signin(user))
    }
  }, [])

  return (
    <BrowserRouter>
      <Navbar></Navbar>

      <Routes>
        <Route path='/' element={<Home />}></Route>
        <Route path='/user-signin' element={<UserSignin />}></Route>
        <Route path='/signup' element={<Signup />}></Route>
        <Route path='/update-profile' element={<UpdateProfile />}></Route>
        <Route path='/add-train' element={<AddTrain />}></Route>
        <Route path='/schedule-train' element={<ScheduleTrain />}></Route>
        <Route path='/update-train' element={<UpdateTrain />}></Route>
        <Route path='/show-alltrain' element={<ShowAllTrains />}></Route>
        <Route path='/admin-signin' element={<AdminSignin />}></Route>
        <Route path='/remove-train' element={<RemoveTrain />}></Route>
        <Route path='/cancel_ticket' element={<CancelTicket />}></Route>
        <Route path='/scheduled_train' element={<BookTicket />}></Route>
        <Route path='/available_trains' element={<AvailableTrains />}></Route>
        <Route path='/booked_ticket' element={<BookedTicketInfo />}></Route>
        <Route path='/show_allTickets' element={<ShowAllTckets />}></Route>
        <Route path='/admin_func' element={<AdminFunctionality />}></Route>
        <Route path='/search_train' element={<SearchTrain />}></Route>
        <Route path='/route_search' element={<TrainRoutes />}></Route>
        <Route path='/cancel_byId' element={<CancelById />}></Route>
        <Route path='/delete_ticketById' element={<DeleteTicketById />}></Route>
      </Routes>
      <ToastContainer position='top-center' autoClose={2000} />
    </BrowserRouter>
  )
}

export default App
