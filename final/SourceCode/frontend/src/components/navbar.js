import { Link, useNavigate } from 'react-router-dom'
import { signin, signout } from '../slices/authSlice'
import { useSelector, useDispatch } from 'react-redux'
import { useEffect } from 'react'

const Navbar = () => {
  const signinStatus = useSelector((state) => state.authSlice.status)
  console.log(signinStatus)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  useEffect(() => {
    console.log('inside useEffect of navbar.js')
    if (sessionStorage['token'] && sessionStorage['token'].length > 0) {
      const user = {
        token: sessionStorage['token'],
        firstName: sessionStorage['username'],
      }
      dispatch(signin(user))
    }
  }, [])

  return (
    <nav
      style={{ backgroundColor: '#e3f2fd' }}
      className='navbar navbar-expand-lg navbar-dark'>
      <div className='container-fluid'>
        <Link
          className='navbar-brand'
          to='/'
          style={{ color: 'black', fontWeight: 'bold' }}>
          ORRS
        </Link>
        <button
          className='navbar-toggler'
          type='button'
          data-bs-toggle='collapse'
          data-bs-target='#navbarSupportedContent'
          aria-controls='navbarSupportedContent'
          aria-expanded='false'
          aria-label='Toggle navigation'>
          <span className='navbar-toggler-icon'></span>
        </button>

        <div className='collapse navbar-collapse'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            <li className='nav-item'>
              <Link
                className='nav-link active'
                aria-current='page'
                to='/'
                style={{ color: 'black', fontWeight: 'bold' }}>
                Home
              </Link>
            </li>

            {/* <li className='nav-item'>
              <Link
                className='nav-link active'
                aria-current='page'
                to='/add-train'>
                Add Train
              </Link>
            </li>
            <li className='nav-item'>
              <Link
                className='nav-link active'
                aria-current='page'
                to='/show-alltrain'>
                Show Trains
              </Link>
            </li> */}
            {signinStatus && (
              <li className='nav-item'>
                <Link
                  className='nav-link active'
                  aria-current='page'
                  to='/scheduled_train'
                  style={{ color: 'black', fontWeight: 'bold' }}>
                  Bookt Ticket
                </Link>
              </li>
            )}
            {signinStatus && (
              <li className='nav-item'>
                <Link
                  className='nav-link active'
                  aria-current='page'
                  to='/search_train'
                  style={{ color: 'black', fontWeight: 'bold' }}>
                  Search Train
                </Link>
              </li>
            )}
            {signinStatus && (
              <li className='nav-item'>
                <Link
                  className='nav-link active'
                  aria-current='page'
                  to='/show_allTickets'
                  style={{ color: 'black', fontWeight: 'bold' }}>
                  Show All Tickets
                </Link>
              </li>
            )}
            {signinStatus && (
              <li className='nav-item'>
                <Link
                  className='nav-link active'
                  aria-current='page'
                  to='/cancel_byId'
                  style={{ color: 'black', fontWeight: 'bold' }}>
                  Cancel ticket
                </Link>
              </li>
            )}
          </ul>

          <ul className='navbar-nav navbar-right'>
            <li className='nav-item'>
              {!signinStatus && (
                <Link
                  className='nav-link active'
                  aria-current='page'
                  to='/user-signin'
                  style={{ color: 'black', fontWeight: 'bold' }}>
                  SignIn
                </Link>
              )}

              {signinStatus && (
                <button
                  style={{
                    textDecoration: 'none',
                    color: 'black',
                    fontWeight: 'bold',
                  }}
                  onClick={() => {
                    // go to signin page
                    navigate('/user-signin')

                    // send the action to let the user signout
                    dispatch(signout())
                  }}
                  className='btn btn-link'
                  aria-current='page'>
                  SignOut
                </button>
              )}
            </li>
          </ul>
          {/* <form className='d-flex' role='search'>
            <input
              className='form-control me-2'
              type='search'
              placeholder='Search'
              aria-label='Search'
            />
            <button className='btn btn-dark' type='submit'>
              Search
            </button>
          </form> */}
        </div>
      </div>
    </nav>
  )
}

export default Navbar
